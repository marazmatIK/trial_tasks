package com.nchernov.trial;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class FileCopier {
    public static long copy(File src, File dst) throws IllegalArgumentException, IOException {
        if (src.isDirectory()) {
            throw new IllegalArgumentException("Dirs are not supported: " + src.getAbsolutePath());
        }
        long start = System.currentTimeMillis();
        Files.copy(src.toPath(), dst.toPath(), REPLACE_EXISTING);
        long end = System.currentTimeMillis();
        return end - start;
    }

    public static void main(String[] args) throws IOException {
        //File src = new File("/home/zugzug/idea_workspace/JavaAlgorithms/trial/src/com/nchernov/trial/tests");
        if (args.length !=2) {
            System.err.println("Wrong args size: " + args.length);
            return;
        }
        File src = new File(args[0]);
        File dst = new File(args[1]);
        long delta = copy(src, dst);
        System.out.println("Files copied in " + delta + " ms");
    }
}
