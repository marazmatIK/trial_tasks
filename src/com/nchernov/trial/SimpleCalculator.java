package com.nchernov.trial;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimpleCalculator {
    private static enum Operation {
        ADD {
            @Override
            int execute(int operandA, int operandB) {
                return operandA + operandB;
            }
        },
        SUB {
            @Override
            int execute(int operandA, int operandB) {
                return operandA - operandB;
            }
        },
        MULTIPLY {
            @Override
            int execute(int operandA, int operandB) {
                return operandA * operandB;
            }
        },
        DIV {
            @Override
            int execute(int operandA, int operandB) {
                return operandA / operandB;
            }
        };
        abstract int execute(int operandA, int operandB);

        static Operation parse(String operation) {
            switch (operation) {
                case "+":
                    return ADD;
                case "-":
                    return SUB;
                case "*":
                    return MULTIPLY;
                case "/":
                    return DIV;
                default:
                    throw new IllegalArgumentException("Unsopported type: " + operation);
            }
        }
    }
    private static class Formula {
        private int operandA;
        private int operandB;
        private Operation operation;

        public Formula(int operandA, int operandB, Operation operation) {
            this.operandA = operandA;
            this.operandB = operandB;
            this.operation = operation;
        }
    }
    private static final Pattern SIMPLE_FORMULA_PATTERN = Pattern.compile("(-?\\d+)(\\+|\\-|\\*|\\/)(\\d+)");
    private static final String OPERATION_REGEX = "[+*/-]";

    public static int eval(String formula) {
        Formula formulaObj = parse(formula);
        return formulaObj.operation.execute(formulaObj.operandA, formulaObj.operandB);
    }

    private static Formula parse(String formula) {
        Matcher matcher = SIMPLE_FORMULA_PATTERN.matcher(formula);
        if (!matcher.find()) {
            throw new IllegalArgumentException("Invalid format: " + formula);
        }
        String operation = matcher.group(2);
        //String tokens[] = formula.split(OPERATION_REGEX, -1);
        return new Formula(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(3)), Operation.parse(operation));
    }

    private static String validateForOperation(String formula) {
        Matcher matcher = SIMPLE_FORMULA_PATTERN.matcher(formula);
        if (!matcher.find()) {
            throw new IllegalArgumentException("Invalid format: " + formula);
        }
        return matcher.group(1);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        while(true) {
            String formula = input.nextLine();
            try {
                System.out.println(eval(formula));
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace();
                System.out.println("Try again\n");
            }
        }
    }

}
