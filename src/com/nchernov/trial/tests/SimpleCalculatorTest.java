package com.nchernov.trial.tests;

import com.nchernov.trial.SimpleCalculator;
/*import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;*/

public class SimpleCalculatorTest {
    /*private static final Map<String, Integer> TEST_CASES = new HashMap<>();
    private static final List<String> ERROR_EXPECTED_TEST_CASES = Arrays.asList("-8//0", "8//0", "6+-5", "35^2", "2A16", "28**6", "-28**6", "28--6", "-28--6", "5++5", "-5++5");
    static {
        TEST_CASES.put("4+7", 11);
        TEST_CASES.put("-4+7", 3);

        TEST_CASES.put("4-7", -3);
        TEST_CASES.put("-4-7", -11);

        TEST_CASES.put("4*7", 28);
        TEST_CASES.put("-4*7", -28);

        TEST_CASES.put("4/7", 0);
        TEST_CASES.put("-4/7", 0);
        TEST_CASES.put("7/4", 1);
        TEST_CASES.put("-7/4", -1);
        TEST_CASES.put("8/4", 2);
        TEST_CASES.put("-8/4", -2);
    }

    @Test
    public void validFormula() {
        for(Map.Entry<String, Integer> testCase: TEST_CASES.entrySet()) {
            assertEquals((int) testCase.getValue(), SimpleCalculator.eval(testCase.getKey()));
        }
    }

    @Test(expected = ArithmeticException.class)
    public void divisionPositiveByZero() {
        SimpleCalculator.eval("5/0");
    }

    @Test(expected = ArithmeticException.class)
    public void divisionNegativeByZero() {
        SimpleCalculator.eval("-5/0");
    }

    @Test
    public void invalidFormulas() {
        for (String testCase : ERROR_EXPECTED_TEST_CASES) {
            try {
                SimpleCalculator.eval(testCase);
                fail("This case should be invalid formula: " + testCase);
            } catch (IllegalArgumentException ex) {
                //ignoring as we expect the error
            }
        }
    }*/
}
