package com.nchernov.trial;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordsCounter {
    private final File file;
    public WordsCounter(File file) {
        this.file = file;
    }

    public int count(String word) throws IOException {
        Pattern re = Pattern.compile(String.format("\\b%s\\b", word));
        int count = 0;
        try (BufferedReader lineReader = new BufferedReader(new FileReader(file))) {
            String line = null;
            while ((line = lineReader.readLine()) != null) {
                Matcher matcher = re.matcher(line);
                while (matcher.find()) {
                    count++;
                }
            }
        }
        return count;
    }

    public static void main(String[] args) throws IOException {
        if (args.length !=2) {
            System.err.println("Wrong args size: " + args.length);
            return;
        }
        File file = new File(args[0]);
        String word = args[1];

        WordsCounter wordsCounter = new WordsCounter(file);
        System.out.println(wordsCounter.count(word));
    }
}
