package com.nchernov.trial;

import java.util.Scanner;

public class FactApp {
    public static int factorial(int number) throws IllegalArgumentException {
        if (number < 0) {
            throw new IllegalArgumentException("Cannot process negative numbers: " + number);
        }
        if (number == 0) {
            return 1;
        }
        int result = 1;
        for (int i = number; i >= 1; i--) {
            result *= i;
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        while(true) {
            int number = input.nextInt();
            try {
                System.out.println(factorial(number));
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace();
                System.out.println("Try again\n");
            }
        }
    }
}
